const canvas = document.querySelector('#canvas');
const ctx = canvas.getContext('2d');
const pen = document.querySelector('#pen');
const eraser = document.querySelector('#eraser');
// const text = document.querySelector('#text');
const rectangle = document.querySelector('#rectangle');
const circle = document.querySelector('#circle');
const triangle = document.querySelector('#triangle');
const refresh = document.querySelector('#refresh');
const redo = document.querySelector('#redo');
const undo = document.querySelector('#undo');
const upload = document.querySelector('#upload');
const download = document.querySelector('#download');
const fontType = document.getElementById('fontType');
const fontSize = document.getElementById('fontSize');
const textInput = document.getElementById('keyin');
const showDiv = document.getElementById("fixed_div");
const checkbox = document.getElementById("checkbox");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
ctx.strokeStyle = '#000000';
ctx.fillStyle = '#000000';
ctx.lineCap = 'round';
ctx.lineWidth = 5;
ctx.font = "12px Times New Roman";
fontType.value = "Times New Roman";
fontSize.value = "12";
var tmpCnvs = new Image();
tmpCnvs.src = canvas.toDataURL();
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

let isDrawing = false;
let isErasing = false;
let isTexting = false;
let isRectangling = false;
let isCircling = false;
let isTriangling = false;
let clickPen = true;
let clickEraser = false;
let clickText = false;
let clickRectangle = false;
let clickCircle = false;
let clickTriangle = false;
let clickRefresh = false;
let clickRedo = false;
let clickUndo = false;
let clickUpload = false;
let clickDownload = false;
let lastX = 0;
let lastY = 0;
let direction = true;
let textX = 0;
let textY = 0;

function mouseMove(e) {
    var x = e.offsetX;
    var y = e.offsetY;
    if (clickPen && isDrawing) {
        ctx.lineCap = 'round';
        ctx.globalCompositeOperation = 'source-over';
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.stroke();
        [lastX, lastY] = [e.offsetX, e.offsetY];
    } else if (clickEraser && isErasing) {
        ctx.globalCompositeOperation = 'destination-out';
        ctx.beginPath();
        ctx.arc(x, y, 10, 0, 2 * Math.PI);
        ctx.fill();
        ctx.lineWidth = 20;
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.stroke();
        [lastX, lastY] = [e.offsetX, e.offsetY];
    } else if (clickRectangle && isRectangling) {
        ctx.globalCompositeOperation = "copy";
        ctx.drawImage(tmpCnvs, 0, 0);
        ctx.globalCompositeOperation = "source-over";
        ctx.beginPath();
        ctx.rect(lastX, lastY, x - lastX, y - lastY);
        ctx.stroke();
        if (checkbox.checked)
            ctx.fill();
    } else if (clickCircle && isCircling) {
        ctx.globalCompositeOperation = "copy";
        ctx.drawImage(tmpCnvs, 0, 0);
        ctx.globalCompositeOperation = "source-over";

        a = ((x - lastX) / 2 > 0) ? (x - lastX) / 2 : (lastX - x) / 2;
        b = ((y - lastY) / 2 > 0) ? (y - lastY) / 2 : (lastY - y) / 2;
        fl_x = (x + lastX) / 2;
        fl_y = (y + lastY) / 2;
        r = (a > b) ? a : b;
        ratioX = a / r;
        ratioY = b / r;
        ctx.save();
        ctx.scale(ratioX, ratioY);
        ctx.beginPath();
        ctx.arc(fl_x / ratioX, fl_y / ratioY, r, 0, 360, false);
        ctx.closePath();
        ctx.stroke();
        ctx.restore();
        if (checkbox.checked)
            ctx.fill();
    } else if (clickTriangle && isTriangling) {
        ctx.globalCompositeOperation = "copy";
        ctx.drawImage(tmpCnvs, 0, 0);
        ctx.globalCompositeOperation = "source-over";

        middleX = (x + lastX) / 2;
        ctx.beginPath();
        ctx.moveTo(lastX, y);
        ctx.lineTo(x, y);
        ctx.lineTo(middleX, lastY);
        ctx.lineTo(lastX, y);
        ctx.closePath();
        ctx.stroke();
        if (checkbox.checked)
            ctx.fill();
    }

    return;
}

pen.addEventListener('click', () => {
    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
eraser.addEventListener('click', () => {
    clickPen = false;
    clickEraser = true;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
text.addEventListener('click', () => {
    clickPen = false;
    clickEraser = false;
    clickText = true;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
rectangle.addEventListener('click', () => {
    clickPen = false;
    clickEraser = false;
    clickText = false;
    clickRectangle = true;
    clickCircle = false;
    clickTriangle = false;
});
circle.addEventListener('click', () => {
    clickPen = false;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = true;
    clickTriangle = false;
});
triangle.addEventListener('click', () => {
    clickPen = false;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = true;
});
refresh.addEventListener('click', () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
redo.addEventListener('click', (e) => {
    window.history.forward();

    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
undo.addEventListener('click', (e) => {
    window.history.back();

    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
fontType.addEventListener('change', () => {
    ctx.font = fontSize.value + "px " + fontType.value;
});
fontSize.addEventListener('change', () => {
    ctx.font = fontSize.value + "px " + fontType.value;
});

function Upload(e) {
    var img = new Image();
    var file = e.files[0];
    if (!file) {
        return;
    }
    img.onload = function () {
        var a = img.width / img.height;
        var img_width;
        var img_height;
        if (a > canvas.width / canvas.height) {
            img_width = canvas.width;
            img_height = img.height * canvas.width / img.width;
        }
        else {
            img_width = img.width * canvas.height / img.height;
            img_height = canvas.height;
        }
        ctx.drawImage(img, 0, 0, img_width, img_height);
    }
    img.src = URL.createObjectURL(file);
    e.value = '';
    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
}

download.addEventListener('click', (e) => {
    var cnvs = document.getElementById("canvas");
    image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "image.png";
    link.href = image;
    link.click();

    clickPen = true;
    clickEraser = false;
    clickText = false;
    clickRectangle = false;
    clickCircle = false;
    clickTriangle = false;
});
window.addEventListener('popstate', (e) => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (e.state) {
        ctx.putImageData(e.state, 0, 0);
    }
});
// $('#keyin').bind('keypress', function (e) {
//     if (e.which == '13') {
//         tool.do($(this).val(), start[0], start[1]);
//         start = [];
//         drawing = false;
//         $(this).css('display', 'none');
//         $(this).val('');
//     }
// });
textInput.addEventListener('keypress', (e) => {
    if (e.which == '13') {
        ctx.fillText(textInput.value, textX, textY);
        textInput.value = "";
    }
});

canvas.addEventListener('mousedown', (e) => {
    [lastX, lastY] = [e.offsetX, e.offsetY];
    if (clickPen) isDrawing = true;
    else if (clickEraser) isErasing = true;
    else if (clickRectangle) {
        tmpCnvs.src = canvas.toDataURL();
        isRectangling = true;
    }
    else if (clickCircle) {
        tmpCnvs.src = canvas.toDataURL();
        isCircling = true;
    }
    else if (clickTriangle) {
        tmpCnvs.src = canvas.toDataURL();
        isTriangling = true;
    } else if (clickText) {
        tmpCnvs.src = canvas.toDataURL();
        textInput.style.font = ctx.font;
        textInput.style.color = ctx.fillStyle;
        // textInput.position = 'absolute';
        textInput.style.top = lastY + 'px';
        textInput.style.left = lastX + 'px';
        textInput.style.width = '300px';
        textInput.style.height = fontSize.value * 5 + 'px';
        textInput.style.display = 'block';
        textInput.style.border = '0px';
        textInput.style.backgroundColor = "transparent";
        setTimeout(function () {
            $('#keyin').focus();
        });
        textX = lastX;
        textY = lastY;
    }

    if (showDiv.style.display !== "none") {
        showDiv.style.display = "none";
    }
});
canvas.addEventListener('mousemove', mouseMove);
canvas.addEventListener('mouseup', (e) => {
    if (clickPen) isDrawing = false;
    else if (clickEraser) isErasing = false;
    else if (clickRectangle) {
        tmpCnvs.src = canvas.toDataURL();
        isRectangling = false;
    }
    else if (clickCircle) {
        tmpCnvs.src = canvas.toDataURL();
        isCircling = false;
    }
    else if (clickTriangle) {
        tmpCnvs.src = canvas.toDataURL();
        isTriangling = false;
    }

    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
});
canvas.addEventListener('mousout', (e) => {
    if (clickPen) isDrawing = false;
    else if (clickEraser) isErasing = false;
    else if (clickRectangle) {
        tmpCnvs.src = canvas.toDataURL();
        isRectangling = false;
    }
    else if (clickCircle) {
        tmpCnvs.src = canvas.toDataURL();
        isCircling = false;
    }
    else if (clickTriangle) {
        tmpCnvs.src = canvas.toDataURL();
        isTriangling = false;
    }

    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
});

// text


// color pickr
$(document).ready(function () {
    const pickr = Pickr.create({
        el: "#color_input",
        theme: "monolith",
        components: {
            preview: true,
            opacity: true,
            hue: true,
            // Input / output Options
            interaction: {
                hex: true,
                rgba: true,
                hsla: true,
                hsva: true,
                cmyk: true,
                input: true,
                clear: true,
                save: true
            }
        }
    });
    //change the color of the main div when color changes
    pickr.on("change", function (e) {
        ctx.strokeStyle = e.toRGBA();
        ctx.fillStyle = e.toRGBA();
        // $("#canvas").css("backgroundColor", e.toRGBA());
    });
});

function show() {
    if (showDiv.style.display !== "none") {
        showDiv.style.display = "none";
    } else {
        showDiv.style.display = "block";
    }
}

function changethickness() {
    thickness = document.getElementById("thickness");
    ctx.lineWidth = thickness.value;
}