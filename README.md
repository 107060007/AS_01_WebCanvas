# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components** | **Score** | **Check** |
| :------------------- | :-------: | :-------: |
| Basic control tools  |    30%    |     Y     |
| Text input           |    10%    |     Y     |
| Cursor icon          |    10%    |     Y     |
| Refresh button       |    10%    |     Y     |

| **Advanced tools**     | **Score** | **Check** |
| :--------------------- | :-------: | :-------: |
| Different brush shapes |    15%    |     Y     |
| Un/Re-do button        |    10%    |     Y     |
| Image tool             |    5%     |     Y     |
| Download               |    5%     |     Y     |

| **Other useful widgets** | **Score** | **Check** |
| :----------------------- | :-------: | :-------: |
| Name of widgets          |   1~5%    |     N     |


---

### How to use 

My canvas is full screen, so you can draw on everywhere. My function button is on the right side of screen, the buttons will hide when drawing. Each button have its own tooltips, function of the button is write on tooltip. To use every canvas tool, you just need to click the button.
* basic control tool
![alt text](GIF/basic.gif)
* text input
![alt text](GIF/text.gif)
* cursor icon
![alt text](GIF/cursor.gif)
* refresh button
![alt text](GIF/refresh.gif)
* different brush shape
![alt text](GIF/different.gif)
* un/re-do button
![alt text](GIF/unredo.gif)
* download image
![alt text](GIF/download.gif)
* upload image
![alt text](GIF/upload.gif)
### Function description

My implementation of pen, rectangle, circle, triangle use html component `canvas` with some api and math computation.
My implementation of eraser is very like pen, but change composite operation from `source-over` to `destination-out`.
My implementation of refresh is just clear the canvas.
My implementation of un-redo use html method `window.history` to remember each window state.
My implementation of uppload and download use html component `<input type="file">` to transfer image file between local and remote.
My implementation of input text use html component `<input type="text">`.
My implementation of color picker use html component `<input type="color">`. But `input` component is not very beautiful, so I use a `button` to contain `input`. Thus, `input` will not show on canvas, just can see `button`. But, the function of `input` is still work.

### Gitlab page link
https://107060007.gitlab.io/AS_01_WebCanvas/

<style>
table th{
    width: 100%;
}
</style>
